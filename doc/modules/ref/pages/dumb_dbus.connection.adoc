= dumb_dbus.connection

ifeval::["{doctype}" == "manpage"]

== Name

dumb_dbus.connection - a D-Bus connection

== Description

endif::[]

[source,lua]
----
local dumb_dbus = require 'dumb_dbus'
----

== Functions

=== `call(self, bus_name: string, object_path: string, interface_name: string, method_name: string, parameters: value[]) -> value`

Invokes the `method_name` method on the `interface_name` D-Bus interface
on the remote object at `object_path` owned by `bus_name`.

//=== `call_with_fds(self, bus_name: string, object_path: string, interface_name: string, method_name: string, parameters: value[], fds: file_descriptor[]) -> value`
//
//Invokes the `method_name` method on the `interface_name` D-Bus interface
//on the remote object at `object_path` owned by `bus_name`.

=== `emit_signal(self, destination_bus_name: string, object_path: string, interface_name: string, signal_name: string, parameters: value[])`

Emits a signal.

=== `signal_subscribe(self, sender: string, interface_name: string, member: string, object_path: string, callback: function) -> integer`

Subscribes to signals on `self` and invokes `callback` whenever the
signal is received.

=== `signal_unsubscribe(self, subscription_id: integer)`

Unsubscribes from signals.
