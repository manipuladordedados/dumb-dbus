= dumb_dbus

:_:

ifeval::["{doctype}" == "manpage"]

== Name

dumb_dbus - manually-written bindings for the D-Bus implementation found in GLib

== Description

endif::[]

[source,lua]
----
local dumb_dbus = require 'dumb_dbus'
----

This module exposes functionality found on Gio's GDBus.

== Properties

=== `system_bus: dumb_dbus.connection`

The system-wide message bus.

=== `session_bus: dumb_dbus.connection`

The login session message bus.

== Bugs

Dumb D-Bus is just a proof-of-concept. The code is small and can be
used to learn about how to create Emilua plugins. However as of now
there isn't interest in developing a robust implementation for serious
usage. The following list describes known-limitations of current's
implementation.

* The returned tuple from method calls will only use up to the first 3
  members. Extra members in the tail will be discarded.
* Not all types are supported. Dumb D-Bus will just use `nil` in these cases.
* Not every type of structural nesting is supported. Dumb D-Bus will
  just ignore the value altogether or use `nil` in many of these cases.
* Dumb D-Bus will not install a `GCancellable` to be triggered on
  fiber interruption.
* Dumb D-Bus will not log runtime errors from the Lua callbacks you register.
* Dumb D-Bus will not handle some rare errors (e.g. LUA_ERRMEM).

Most of these limitations are just boring to deal with. The
concurrency model itself (e.g. memory management, thread
synchronization and event orchestration) should be very robust. So you
can definitively do use the code base to study about Emilua plugin
development.

Furthermore if you're just interested in creating a small script to
automate a non-critical task, this plugin should be useful for you
besides its simplicity.
