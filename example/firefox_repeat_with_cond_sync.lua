local condition_variable = require 'condition_variable'
local dumb_dbus = require 'dumb_dbus'
local mutex = require 'mutex'

local bus = dumb_dbus.session_bus
local name = 'org.mpris.MediaPlayer2.firefox.instance5246'

local last_signal_arrived_id = 0
local last_signal_arrived_id_mtx = mutex.new()
local last_signal_arrived_id_changed_cond = condition_variable.new()

local function callback(sender, object_path, interface, signal, params)
    if params[1] ~= 'org.mpris.MediaPlayer2.Player' then
        return
    end
    last_signal_arrived_id = last_signal_arrived_id + 1
    last_signal_arrived_id_changed_cond:notify_all()
end

bus:signal_subscribe(
    name,
    'org.freedesktop.DBus.Properties', 'PropertiesChanged',
    '/org/mpris/MediaPlayer2',
    callback)

last_signal_arrived_id_mtx:lock()
local last_signal_seen_id = last_signal_arrived_id
while true do
    while last_signal_seen_id == last_signal_arrived_id do
        last_signal_arrived_id_changed_cond:wait(last_signal_arrived_id_mtx)
    end
    last_signal_seen_id = last_signal_arrived_id
    local status = bus:call(name,
         '/org/mpris/MediaPlayer2',
         'org.freedesktop.DBus.Properties', 'Get',
         {'org.mpris.MediaPlayer2.Player', 'PlaybackStatus'})
    if status ~= 'Playing' then
        bus:call(
            name,
            '/org/mpris/MediaPlayer2',
            'org.mpris.MediaPlayer2.Player', 'Play')
    end
end
