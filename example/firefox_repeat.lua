local dumb_dbus = require 'dumb_dbus'

local bus = dumb_dbus.session_bus
local name = 'org.mpris.MediaPlayer2.firefox.instance5246'

local function callback(sender, object_path, interface, signal, params)
    if params[1] ~= 'org.mpris.MediaPlayer2.Player' then
        return
    end

    -- `callback` runs in an unspecified fiber where suspension is
    -- forbidden. Therefore we can't call `bus:call()` directly. To work around
    -- this limitation, we spawn a new fiber to handle the event.
    spawn(function()
        local status = bus:call(name,
             '/org/mpris/MediaPlayer2',
             'org.freedesktop.DBus.Properties', 'Get',
             {'org.mpris.MediaPlayer2.Player', 'PlaybackStatus'})
        if status ~= 'Playing' then
            bus:call(
                name,
                '/org/mpris/MediaPlayer2',
                'org.mpris.MediaPlayer2.Player', 'Play')
        end
    end):detach()
end

bus:signal_subscribe(
    name,
    'org.freedesktop.DBus.Properties', 'PropertiesChanged',
    '/org/mpris/MediaPlayer2',
    callback)
