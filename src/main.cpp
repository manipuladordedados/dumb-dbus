// NOTE: As an exception to the usual GLib rule that a particular
// object must not be used by two threads at the same time,
// GDBusConnection‘s methods may be called from any thread. This is so
// that g_bus_get() and g_bus_get_sync() can safely return the same
// GDBusConnection when called from any thread.

EMILUA_GPERF_DECLS_BEGIN(includes)
#include <emilua/plugin.hpp>
#include <emilua/async_base.hpp>

#include <emilua_glib/service.hpp>
#include <gio/gio.h>
#include <glibmm/error.h>
#include <glibmm/variant.h>
#include <boost/asio/steady_timer.hpp>
EMILUA_GPERF_DECLS_END(includes)

EMILUA_GPERF_DECLS_BEGIN(dumb_dbus)
namespace asio = boost::asio;
namespace hana = boost::hana;

static char connection_mt_key;
static char connection_call_key;
EMILUA_GPERF_DECLS_END(dumb_dbus)

static const char module_mt_index_source[] = R"lua(
local error, system_bus, session_bus = ...

return function(_, key)
    if key == "system_bus" then
        local e, r = system_bus()
        if e then error(e) end
        return r
    elseif key == "session_bus" then
        local e, r = session_bus()
        if e then error(e) end
        return r
    end
end
)lua";

struct bus_get_operation
{
    bus_get_operation(std::shared_ptr<emilua::vm_context> vm_ctx,
                      lua_State* fiber, emilua_glib::service& service)
        : vm_ctx{std::move(vm_ctx)}
        , fiber{fiber}
        , guard{service}
    {}

    std::shared_ptr<emilua::vm_context> vm_ctx;
    lua_State* fiber;
    emilua_glib::service::work_guard guard;
};

static void bus_get_callback(GObject* source_object, GAsyncResult* res,
                             gpointer data)
{
    std::unique_ptr<bus_get_operation> op(
        static_cast<bus_get_operation*>(data));
    GError *error = NULL;
    auto conn = g_bus_get_finish(res, &error);
    if (error) {
        Glib::Error e2{error};
        op->vm_ctx->strand().post(
            [vm_ctx=op->vm_ctx,current_fiber=op->fiber,error=e2]() {
                auto err_pusher = [&error](lua_State* L) {
                    lua_createtable(L, /*narr=*/0, /*nrec=*/3);

                    lua_pushliteral(L, "domain");
                    lua_pushinteger(L, error.domain());
                    lua_rawset(L, -3);

                    lua_pushliteral(L, "code");
                    lua_pushinteger(L, error.code());
                    lua_rawset(L, -3);

                    lua_pushliteral(L, "message");
                    lua_pushstring(L, error.what());
                    lua_rawset(L, -3);
                };
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        hana::make_pair(
                            emilua::vm_context::options::arguments,
                            hana::make_tuple(err_pusher)))
                );
            }, std::allocator<void>{});
        return;
    }
    op->vm_ctx->strand().post(
        [vm_ctx=op->vm_ctx,current_fiber=op->fiber,conn]() {
            auto conn_pusher = [conn](lua_State* L) {
                GDBusConnection** conn_ud = reinterpret_cast<
                    GDBusConnection**>(lua_newuserdata(L, sizeof(void*)));
                emilua::rawgetp(L, LUA_REGISTRYINDEX, &connection_mt_key);
                emilua::setmetatable(L, -2);
                *conn_ud = conn;
            };
            vm_ctx->fiber_resume(
                current_fiber,
                hana::make_set(
                    hana::make_pair(
                        emilua::vm_context::options::arguments,
                        hana::make_tuple(std::nullopt, conn_pusher)))
            );
        }, std::allocator<void>{});
}

template<GBusType BUS_TYPE>
static int get_bus(lua_State* L)
{
    auto vm_ctx = emilua::get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& service = asio::use_service<emilua_glib::service>(
        vm_ctx->strand().context());

    service.dispatch_work_with_thread_default(
        [vm_ctx,current_fiber,&service]() {
            auto userdata = new bus_get_operation(
                vm_ctx, current_fiber, service);
            g_bus_get(BUS_TYPE, NULL, bus_get_callback, userdata);
        }
    );

    return lua_yield(L, 0);
}

EMILUA_GPERF_DECLS_BEGIN(connection)
struct call_operation
{
    call_operation(std::shared_ptr<emilua::vm_context> vm_ctx,
                   lua_State* fiber, emilua_glib::service& service,
                   std::shared_ptr<GDBusConnection> connection)
        : vm_ctx{std::move(vm_ctx)}
        , fiber{fiber}
        , guard{service}
        , connection{connection}
    {}

    std::shared_ptr<emilua::vm_context> vm_ctx;
    lua_State* fiber;
    emilua_glib::service::work_guard guard;
    std::shared_ptr<GDBusConnection> connection;
};

static void call_callback(GObject *source_object, GAsyncResult *res,
                          gpointer data)
{
    std::unique_ptr<call_operation> op(static_cast<call_operation*>(data));
    GError* error = NULL;
    GVariant* ret = g_dbus_connection_call_finish(
        op->connection.get(), res, &error);
    if (!ret) {
        Glib::Error e2{error};
        op->vm_ctx->strand().post(
            [vm_ctx=op->vm_ctx,current_fiber=op->fiber,error=e2]() {
                auto err_pusher = [&error](lua_State* L) {
                    lua_createtable(L, /*narr=*/0, /*nrec=*/3);

                    lua_pushliteral(L, "domain");
                    lua_pushinteger(L, error.domain());
                    lua_rawset(L, -3);

                    lua_pushliteral(L, "code");
                    lua_pushinteger(L, error.code());
                    lua_rawset(L, -3);

                    lua_pushliteral(L, "message");
                    lua_pushstring(L, error.what());
                    lua_rawset(L, -3);
                };
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        hana::make_pair(
                            emilua::vm_context::options::arguments,
                            hana::make_tuple(err_pusher)))
                );
            }, std::allocator<void>{});
        return;
    }

    Glib::VariantBase ret2{ret};
    op->vm_ctx->strand().post(
        [vm_ctx=op->vm_ctx,current_fiber=op->fiber,ret=ret2]() {
            using Glib::VariantBase;
            using Glib::Variant;
            using emilua::push;

            static constexpr auto remove_variant_type = [](VariantBase& v) {
                if (v.classify() != G_VARIANT_CLASS_VARIANT)
                    return;

                auto v2 = VariantBase::cast_dynamic<Glib::VariantContainerBase>(
                    v);
                if (v2.get_n_children() != 1)
                    return;

                v = v2.get_child();
            };

            std::vector<Glib::VariantBase> tuple;
            if (ret.classify() == G_VARIANT_CLASS_TUPLE) {
                auto v = VariantBase::cast_dynamic<Glib::VariantContainerBase>(
                    ret);
                for (gsize i = 0, size = v.get_n_children() ; i != size ; ++i) {
                    tuple.emplace_back(v.get_child(i));
                    remove_variant_type(tuple.back());
                }
            } else {
                tuple.emplace_back(ret);
                remove_variant_type(tuple.back());
            }

            static constexpr auto push_v = [](lua_State* L, VariantBase& ret) {
                switch (ret.classify()) {
                case G_VARIANT_CLASS_BOOLEAN: {
                    auto v = VariantBase::cast_dynamic<Variant<bool>>(ret);
                    lua_pushboolean(L, v.get());
                    break;
                }
                case G_VARIANT_CLASS_BYTE: {
                    auto v = VariantBase::cast_dynamic<Variant<unsigned char>>(
                        ret);
                    lua_pushinteger(L, v.get());
                    break;
                }
                case G_VARIANT_CLASS_INT16: {
                    auto v = VariantBase::cast_dynamic<Variant<gint16>>(ret);
                    lua_pushinteger(L, v.get());
                    break;
                }
                case G_VARIANT_CLASS_UINT16: {
                    auto v = VariantBase::cast_dynamic<Variant<guint16>>(ret);
                    lua_pushinteger(L, v.get());
                    break;
                }
                case G_VARIANT_CLASS_INT32: {
                    auto v = VariantBase::cast_dynamic<Variant<gint32>>(ret);
                    lua_pushinteger(L, v.get());
                    break;
                }
                case G_VARIANT_CLASS_UINT32: {
                    auto v = VariantBase::cast_dynamic<Variant<guint32>>(ret);
                    lua_pushinteger(L, v.get());
                    break;
                }
                case G_VARIANT_CLASS_INT64: {
                    auto v = VariantBase::cast_dynamic<Variant<gint64>>(ret);
                    lua_pushinteger(L, v.get());
                    break;
                }
                case G_VARIANT_CLASS_UINT64: {
                    auto v = VariantBase::cast_dynamic<Variant<guint64>>(ret);
                    lua_pushinteger(L, v.get());
                    break;
                }
                case G_VARIANT_CLASS_DOUBLE: {
                    auto v = VariantBase::cast_dynamic<Variant<double>>(ret);
                    lua_pushnumber(L, v.get());
                    break;
                }
                case G_VARIANT_CLASS_STRING: {
                    auto v = VariantBase::cast_dynamic<Variant<Glib::ustring>>(
                        ret);
                    push(L, static_cast<std::string>(v.get()));
                    break;
                }
                // TODO:
                //case G_VARIANT_CLASS_ARRAY:
                //case G_VARIANT_CLASS_TUPLE:
                //case G_VARIANT_CLASS_DICT_ENTRY:
                default:
                    lua_pushnil(L);
                }
            };

            auto args_pusher = [&tuple](lua_State* L) -> int {
                lua_pushnil(L);
                for (auto& v : tuple) {
                    push_v(L, v);
                }
                return tuple.size() + 1;
            };

            if (!lua_checkstack(current_fiber, tuple.size() + 1)) {
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            }
            vm_ctx->fiber_resume(
                current_fiber,
                hana::make_set(
                    hana::make_pair(
                        emilua::vm_context::options::variadic_arguments,
                        args_pusher))
            );
        }, std::allocator<void>{});
}

static int conn_call(lua_State* L)
{
    using emilua::push;
    using emilua::tostringview;

    lua_settop(L, 6);

    auto vm_ctx = emilua::get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto conn = static_cast<GDBusConnection**>(lua_touserdata(L, 1));
    if (!conn || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    emilua::rawgetp(L, LUA_REGISTRYINDEX, &connection_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    g_object_ref(*conn);
    std::shared_ptr<GDBusConnection> conn2{*conn, [](GDBusConnection* obj) {
        g_object_unref(obj);
    }};
    std::string bus_name{tostringview(L, 2)};
    std::string object_path{tostringview(L, 3)};
    std::string interface_name{tostringview(L, 4)};
    std::string method_name{tostringview(L, 5)};

    std::vector<Glib::VariantBase> parameters;
    switch (lua_type(L, 6)) {
    case LUA_TNIL:
        break;
    case LUA_TTABLE: {
        bool run = true;
        for (int i = 1 ; run ; ++i) {
            lua_rawgeti(L, 6, i);
            switch (lua_type(L, -1)) {
            case LUA_TNIL:
                run = false;
                break;
            case LUA_TBOOLEAN:
                parameters.emplace_back(Glib::Variant<bool>::create(
                    lua_toboolean(L, -1)));
                break;
            case LUA_TNUMBER:
                parameters.emplace_back(Glib::Variant<double>::create(
                    lua_tonumber(L, -1)));
                break;
            case LUA_TSTRING:
                parameters.emplace_back(Glib::Variant<Glib::ustring>::create(
                    lua_tostring(L, -1)));
                break;
            default:
                push(L, std::errc::invalid_argument, "arg", 6);
                return lua_error(L);
            }
            lua_pop(L, 1);
        }
        break;
    }
    default:
        push(L, std::errc::invalid_argument, "arg", 6);
        return lua_error(L);
    }

    auto parameters2 = Glib::VariantContainerBase::create_tuple(parameters);

    auto& service = asio::use_service<emilua_glib::service>(
        vm_ctx->strand().context());

    service.dispatch_work_with_thread_default(
        [
            vm_ctx,current_fiber,&service,
            conn=conn2,bus_name,object_path,interface_name,method_name,
            parameters=parameters2
        ]() mutable {
            // TODO: set fiber interrupter
            GCancellable* cancellable = NULL;

            auto userdata = new call_operation(
                vm_ctx, current_fiber, service, conn);
            g_dbus_connection_call(
                conn.get(), bus_name.data(), object_path.data(),
                interface_name.data(), method_name.data(), parameters.gobj(),
                /*reply_type=*/NULL, G_DBUS_CALL_FLAGS_NONE,
                /*timeout_msec=*/G_MAXINT, cancellable, call_callback,
                userdata);
        }
    );

    return lua_yield(L, 0);
}

static int conn_emit_signal(lua_State* L)
{
    using emilua::push;
    using emilua::tostringview;

    lua_settop(L, 6);

    auto conn = static_cast<GDBusConnection**>(lua_touserdata(L, 1));
    if (!conn || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    emilua::rawgetp(L, LUA_REGISTRYINDEX, &connection_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    auto destination_bus_name = tostringview(L, 2);
    auto object_path = tostringview(L, 3);
    auto interface_name = tostringview(L, 4);
    auto signal_name = tostringview(L, 5);

    std::vector<Glib::VariantBase> parameters;
    switch (lua_type(L, 6)) {
    case LUA_TNIL:
        break;
    case LUA_TTABLE: {
        bool run = true;
        for (int i = 1 ; run ; ++i) {
            lua_rawgeti(L, 6, i);
            switch (lua_type(L, -1)) {
            case LUA_TNIL:
                run = false;
                break;
            case LUA_TBOOLEAN:
                parameters.emplace_back(Glib::Variant<bool>::create(
                    lua_toboolean(L, -1)));
                break;
            case LUA_TNUMBER:
                parameters.emplace_back(Glib::Variant<double>::create(
                    lua_tonumber(L, -1)));
                break;
            case LUA_TSTRING:
                parameters.emplace_back(Glib::Variant<Glib::ustring>::create(
                    lua_tostring(L, -1)));
                break;
            default:
                push(L, std::errc::invalid_argument, "arg", 6);
                return lua_error(L);
            }
            lua_pop(L, 1);
        }
        break;
    }
    default:
        push(L, std::errc::invalid_argument, "arg", 6);
        return lua_error(L);
    }

    auto parameters2 = Glib::VariantContainerBase::create_tuple(parameters);

    GError *error = NULL;
    gboolean ok = g_dbus_connection_emit_signal(
        *conn, destination_bus_name.data(), object_path.data(),
        interface_name.data(), signal_name.data(), parameters2.gobj(), &error);
    if (!ok) {
        if (!error) {
            return luaL_error(L, "unknown error");
        }

        Glib::Error e2{error};

        lua_createtable(L, /*narr=*/0, /*nrec=*/3);
        {
            lua_pushliteral(L, "domain");
            lua_pushinteger(L, e2.domain());
            lua_rawset(L, -3);

            lua_pushliteral(L, "code");
            lua_pushinteger(L, e2.code());
            lua_rawset(L, -3);

            lua_pushliteral(L, "message");
            lua_pushstring(L, e2.what());
            lua_rawset(L, -3);
        }
        return lua_error(L);
    }
    return 0;
}

struct signal_subscription
{
    signal_subscription(std::shared_ptr<emilua::vm_context> vm_ctx,
                        int function, emilua_glib::service& service)
        : vm_ctx{std::move(vm_ctx)}
        , function{function}
        , guard{service}
    {}

    ~signal_subscription()
    {
        vm_ctx->strand().post([vm_ctx=this->vm_ctx,function=this->function]() {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            luaL_unref(L, LUA_REGISTRYINDEX, function);
        }, std::allocator<void>{});
    }

    std::shared_ptr<emilua::vm_context> vm_ctx;
    int function;
    emilua_glib::service::work_guard guard;
};

static void signal_callback(GDBusConnection *connection,
                            const gchar* sender_name,
                            const gchar* object_path,
                            const gchar* interface_name,
                            const gchar* signal_name,
                            GVariant* parameters,
                            gpointer data)
{
    using Glib::VariantBase;
    using Glib::Variant;
    using Glib::VariantContainerBase;
    using emilua::push;

    VariantBase parameters2{parameters, /*make_a_copy=*/true};

    std::optional<std::string> sender;
    if (sender_name) sender.emplace(sender_name);

    auto op = static_cast<signal_subscription*>(data);
    op->vm_ctx->strand().post(
        [
            vm_ctx=op->vm_ctx,function=op->function,
            sender,
            object_path=std::string{object_path},
            interface=std::string{interface_name},
            signal=std::string{signal_name},
            parameters=parameters2
        ]() mutable {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            lua_rawgeti(L, LUA_REGISTRYINDEX, function);
            try {
                if (sender) {
                    push(L, *sender);
                } else {
                    lua_pushnil(L);
                }
                push(L, object_path);
                push(L, interface);
                push(L, signal);

                lua_newtable(L);
                if (parameters.classify() == G_VARIANT_CLASS_TUPLE) {
                    auto c = VariantBase::cast_dynamic<VariantContainerBase>(
                        parameters);
                    for (
                        gsize i = 0, size = c.get_n_children() ; i != size ; ++i
                    ) {
                        auto ret = c.get_child(i);
                        switch (ret.classify()) {
                        case G_VARIANT_CLASS_BOOLEAN: {
                            auto v = VariantBase::cast_dynamic<Variant<bool>>(
                                ret);
                            lua_pushboolean(L, v.get());
                            break;
                        }
                        case G_VARIANT_CLASS_BYTE: {
                            auto v = VariantBase::cast_dynamic<
                                Variant<unsigned char>>(ret);
                            lua_pushinteger(L, v.get());
                            break;
                        }
                        case G_VARIANT_CLASS_INT16: {
                            auto v = VariantBase::cast_dynamic<
                                Variant<gint16>>(ret);
                            lua_pushinteger(L, v.get());
                            break;
                        }
                        case G_VARIANT_CLASS_UINT16: {
                            auto v = VariantBase::cast_dynamic<
                                Variant<guint16>>(ret);
                            lua_pushinteger(L, v.get());
                            break;
                        }
                        case G_VARIANT_CLASS_INT32: {
                            auto v = VariantBase::cast_dynamic<
                                Variant<gint32>>(ret);
                            lua_pushinteger(L, v.get());
                            break;
                        }
                        case G_VARIANT_CLASS_UINT32: {
                            auto v = VariantBase::cast_dynamic<
                                Variant<guint32>>(ret);
                            lua_pushinteger(L, v.get());
                            break;
                        }
                        case G_VARIANT_CLASS_INT64: {
                            auto v = VariantBase::cast_dynamic<
                                Variant<gint64>>(ret);
                            lua_pushinteger(L, v.get());
                            break;
                        }
                        case G_VARIANT_CLASS_UINT64: {
                            auto v = VariantBase::cast_dynamic
                                <Variant<guint64>>(ret);
                            lua_pushinteger(L, v.get());
                            break;
                        }
                        case G_VARIANT_CLASS_DOUBLE: {
                            auto v = VariantBase::cast_dynamic<
                                Variant<double>>(ret);
                            lua_pushnumber(L, v.get());
                            break;
                        }
                        case G_VARIANT_CLASS_STRING: {
                            auto v = VariantBase::cast_dynamic<
                                Variant<Glib::ustring>>(ret);
                            push(L, static_cast<std::string>(v.get()));
                            break;
                        }
                        // TODO:
                        //case G_VARIANT_CLASS_ARRAY:
                        //case G_VARIANT_CLASS_TUPLE:
                        //case G_VARIANT_CLASS_DICT_ENTRY:
                        default:
                            lua_pushnil(L);
                        }
                        lua_rawseti(L, -2, i + 1);
                    }
                }
            } catch (...) {
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            }
            int res = lua_pcall(L, 5, 0, 0);
            if (res == LUA_ERRMEM) {
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            }
        }, std::allocator<void>{});
}

static int conn_signal_subscribe(lua_State* L)
{
    using emilua::push;
    using emilua::tostringview;

    lua_settop(L, 6);
    luaL_checktype(L, 6, LUA_TFUNCTION);

    auto vm_ctx = emilua::get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& service = asio::use_service<emilua_glib::service>(
        vm_ctx->strand().context());

    auto conn = static_cast<GDBusConnection**>(lua_touserdata(L, 1));
    if (!conn || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    emilua::rawgetp(L, LUA_REGISTRYINDEX, &connection_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    g_object_ref(*conn);
    std::shared_ptr<GDBusConnection> conn2{*conn, [](GDBusConnection* obj) {
        g_object_unref(obj);
    }};
    std::optional<std::string> sender;
    std::optional<std::string> interface_name;
    std::optional<std::string> member;
    std::optional<std::string> object_path;
    lua_pushvalue(L, 6);
    int function = luaL_ref(L, LUA_REGISTRYINDEX);

    switch (lua_type(L, 2)) {
    case LUA_TNIL:
        break;
    case LUA_TSTRING:
        sender.emplace(tostringview(L, 2));
        break;
    default:
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    }

    switch (lua_type(L, 3)) {
    case LUA_TNIL:
        break;
    case LUA_TSTRING:
        interface_name.emplace(tostringview(L, 3));
        break;
    default:
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    }

    switch (lua_type(L, 4)) {
    case LUA_TNIL:
        break;
    case LUA_TSTRING:
        member.emplace(tostringview(L, 4));
        break;
    default:
        push(L, std::errc::invalid_argument, "arg", 4);
        return lua_error(L);
    }

    switch (lua_type(L, 5)) {
    case LUA_TNIL:
        break;
    case LUA_TSTRING:
        object_path.emplace(tostringview(L, 5));
        break;
    default:
        push(L, std::errc::invalid_argument, "arg", 5);
        return lua_error(L);
    }

    service.dispatch_work_with_thread_default(
        [
            vm_ctx,current_fiber,&service,
            conn=conn2,function,sender,interface_name,member,object_path
        ]() {
            auto userdata = new signal_subscription(vm_ctx, function, service);
            auto free_ud = [](gpointer data) {
                delete static_cast<signal_subscription*>(data);
            };

            const gchar* senderr = NULL;
            const gchar* interface_namer = NULL;
            const gchar* memberr = NULL;
            const gchar* object_pathr = NULL;

            if (sender) senderr = sender->data();
            if (interface_name) interface_namer = interface_name->data();
            if (member) memberr = member->data();
            if (object_path) object_pathr = object_path->data();

            auto ret = g_dbus_connection_signal_subscribe(
                conn.get(), senderr, interface_namer, memberr, object_pathr,
                /*arg0=*/NULL, G_DBUS_SIGNAL_FLAGS_NONE, signal_callback,
                userdata, free_ud);

            vm_ctx->strand().post([vm_ctx,current_fiber,ret]() {
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        emilua::vm_context::options::skip_clear_interrupter,
                        hana::make_pair(
                            emilua::vm_context::options::arguments,
                            hana::make_tuple(ret)))
                );
            }, std::allocator<void>{});
        }
    );

    return lua_yield(L, 0);
}

static  int conn_signal_unsubscribe(lua_State* L)
{
    using emilua::push;

    lua_settop(L, 2);
    luaL_checktype(L, 2, LUA_TNUMBER);

    auto conn = static_cast<GDBusConnection**>(lua_touserdata(L, 1));
    if (!conn || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    emilua::rawgetp(L, LUA_REGISTRYINDEX, &connection_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    g_dbus_connection_signal_unsubscribe(*conn, lua_tointeger(L, 2));
    return 0;
}
EMILUA_GPERF_DECLS_END(connection)

static int conn_mt_index(lua_State* L)
{
    auto key = emilua::tostringview(L, 2);
    return EMILUA_GPERF_BEGIN(key)
        EMILUA_GPERF_PARAM(int (*action)(lua_State*))
        EMILUA_GPERF_DEFAULT_VALUE([](lua_State* L) -> int {
            push(L, emilua::errc::bad_index, "index", 2);
            return lua_error(L);
        })
        EMILUA_GPERF_PAIR(
            "call",
            [](lua_State* L) -> int {
                rawgetp(L, LUA_REGISTRYINDEX, &connection_call_key);
                return 1;
            }
        )
        EMILUA_GPERF_PAIR(
            "emit_signal",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, conn_emit_signal);
                return 1;
            }
        )
        EMILUA_GPERF_PAIR(
            "signal_subscribe",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, conn_signal_subscribe);
                return 1;
            }
        )
        EMILUA_GPERF_PAIR(
            "signal_unsubscribe",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, conn_signal_unsubscribe);
                return 1;
            }
        )
    EMILUA_GPERF_END(key)(L);
}

static int conn_mt_gc(lua_State* L)
{
    auto conn = static_cast<GDBusConnection**>(lua_touserdata(L, 1));
    g_object_unref(*conn);
    return 0;
}

class dbus_plugin : emilua::plugin
{
public:
    std::error_code init_ioctx_services(
        asio::io_context& ioctx) noexcept override
    {
        asio::use_service<emilua_glib::service>(ioctx);
        return {};
    }

    std::error_code init_lua_module(
        emilua::vm_context& vm_ctx, lua_State* L) override
    {
        (void)vm_ctx;

        lua_newuserdata(L, 0);
        lua_newtable(L);
        {
            lua_pushliteral(L, "__metatable");
            lua_pushliteral(L, "dumb_dbus");
            lua_rawset(L, -3);

            lua_pushliteral(L, "__index");
            {
                // TODO: handle errors
                luaL_loadbuffer(L, module_mt_index_source,
                                sizeof(module_mt_index_source) - 1, NULL);
                lua_getglobal(L, "error");
                lua_pushcfunction(L, get_bus<G_BUS_TYPE_SYSTEM>);
                lua_pushcfunction(L, get_bus<G_BUS_TYPE_SESSION>);
                lua_call(L, 3, 1);
            }
            lua_rawset(L, -3);
        }
        emilua::setmetatable(L, -2);

        lua_pushlightuserdata(L, &connection_mt_key);
        {
            lua_newtable(L);

            lua_pushliteral(L, "__metatable");
            lua_pushliteral(L, "dumb_dbus.connection");
            lua_rawset(L, -3);

            lua_pushliteral(L, "__index");
            lua_pushcfunction(L, conn_mt_index);
            lua_rawset(L, -3);

            lua_pushliteral(L, "__gc");
            lua_pushcfunction(L, conn_mt_gc);
            lua_rawset(L, -3);
        }
        lua_rawset(L, LUA_REGISTRYINDEX);

        // TODO: emilua should have an async_base to forward the whole tail
        lua_pushlightuserdata(L, &connection_call_key);
        emilua::rawgetp(
            L, LUA_REGISTRYINDEX,
            &emilua::var_args__retval1_to_error__fwd_retval234__key);
        emilua::rawgetp(L, LUA_REGISTRYINDEX, &emilua::raw_error_key);
        lua_pushcfunction(L, conn_call);
        lua_call(L, 2, 1);
        lua_rawset(L, LUA_REGISTRYINDEX);

        return {};
    }
};

extern "C" BOOST_SYMBOL_EXPORT dbus_plugin EMILUA_PLUGIN_SYMBOL;
dbus_plugin EMILUA_PLUGIN_SYMBOL;
